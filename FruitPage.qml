import QtQuick 2.0

Item {
      id: page1
        Rectangle {
            height: 50
            width: parent.width
            opacity: 0.8
            color: "white"
            Text {
                text: "Eco butique"
                horizontalAlignment: Text.AlignHCenter
                font.family: "Times New Roman"
                font.italic: true
                font.bold: true
                font.pixelSize: 48
                color: "black"
                width: parent.width
                height: parent.height
            }

        }

        Grid {
            anchors.rightMargin: 30
            anchors.leftMargin:  30
            anchors.bottomMargin: 70
            anchors.topMargin: 70
            anchors.fill: parent
            spacing: 22
            columns: 4
            rows: 3
            FruitButton {
                source: "http://www.pngall.com/wp-content/uploads/2016/04/Apple-Fruit-PNG-HD.png"
            }

            FruitButton {
                source: "http://www.pngall.com/wp-content/uploads/2016/05/Strawberry-Download-PNG.png"
            }
            FruitButton {
                source: "http://www.pngall.com/wp-content/uploads/2016/05/Persimmon-PNG-File.png"
            }
            FruitButton {
                source: "http://www.pngall.com/wp-content/uploads/2016/05/Lemon-PNG-Image.png"
            }
            FruitButton {
                source: "http://www.pngall.com/wp-content/uploads/2016/05/Avocado-Free-PNG-Image.png"
            }
            FruitButton {
                source: "http://www.pngall.com/wp-content/uploads/2016/05/Pineapple-Free-PNG-Image.png"
            }
            FruitButton {
                source: "http://www.pngall.com/wp-content/uploads/2016/04/Banana-PNG-HD.png"
            }
            FruitButton {
                source: "http://www.pngall.com/wp-content/uploads/2016/04/Kiwi-PNG-Clipart.png"
                onClicked: window.source = source
            }
        }

        Rectangle {
            y: parent.height - 50
            x: (parent.width-200)/2
            height: 50
            width: 200
            opacity: 0.8
            color: "white"
            Text {
                text: "Pay here"
                horizontalAlignment: Text.AlignHCenter
                font.family: "Times New Roman"
                font.italic: true
                font.bold: true
                font.pixelSize: 48
                color: "black"
                width: parent.width
                height: parent.height
            }
            MouseArea {
                anchors.fill: parent
                onClicked: stack.push(Qt.resolvedUrl("Pay.qml"))
            }

        }
}
