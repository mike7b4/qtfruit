import QtQuick 2.0

Item {
    anchors.fill: parent


    Column {
        anchors.centerIn: parent
        spacing: 16
        Text {
            width: parent.width
            height: 40
            font.pixelSize: 32
            color: "white"
            text: "0.5 Kg"
            horizontalAlignment: Text.AlignHCenter
        }
        Text {
            width: parent.width
            height: 40
            font.pixelSize: 32
            color: "white"
            text: "3 mBTC"
            horizontalAlignment: Text.AlignHCenter
        }
        Text {
            width: parent.width
            height: 40
            color: "white"
            font.pixelSize: 32

            text: "Scan the barcode below"
            horizontalAlignment: Text.AlignHCenter
        }

        Image {
            width: 128
            height: 128
            source: window.source
        }
        FruitButton {
            source: "barcode.png"
            onClicked: stack.pop()

        }
     }
}
