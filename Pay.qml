import QtQuick 2.0

Item {
    anchors.fill: parent


    Column {
        anchors.centerIn: parent
        spacing: 16
        Text {
            width: parent.width
            height: 40
            font.pixelSize: 32
            color: "white"
            text: "Pay for: 0.5 Kg Kiwi"
            horizontalAlignment: Text.AlignHCenter
        }
        Text {
            width: parent.width
            height: 40
            font.pixelSize: 32
            color: "white"
            text: "0.3 mBTC"
            horizontalAlignment: Text.AlignHCenter
        }
        Text {
            width: parent.width
            height: 40
            color: "white"
            font.pixelSize: 32

            text: "Scan the barcode below if you like this idea ;)"
            horizontalAlignment: Text.AlignHCenter
        }

        FruitButton {
            source: "barcode_pay.png"
            onClicked: stack.pop()

        }
     }
}
