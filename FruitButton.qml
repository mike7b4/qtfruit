import QtQuick 2.0

Rectangle {
    width: 220
    height: 220
    opacity: 0.8
    color: "lightgreen"
    property string source: ""
    signal clicked
    Image {
        anchors.centerIn: parent
        width: 200
        height: 200
        source: parent.source
    }

    MouseArea {
        anchors.fill: parent
        onClicked: parent.clicked()
    }
}
