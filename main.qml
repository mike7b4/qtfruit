import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.0

Window {
    id: window
    visible: true
    width: 1024
    height: 600
    title: qsTr("Eco Butique")
    property string source: ""
    onSourceChanged: stack.push(Qt.resolvedUrl("FruitAccept.qml"))
        Image {
            anchors.fill: parent
            source: "file://home/mikael/Downloads/Nature 444 photos.jpg"
        }


    StackView {
        id: stack
        initialItem: FruitPage { id: page1 }
        anchors.fill: parent
    }


}
